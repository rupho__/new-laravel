<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/register', function(){
//     return view('register');
// });

// Route::get('/welcome', function () {
//     return view('welcome');
// });

// Route::get('/welcome_second', function () {
//     return view('welcome_second');
// });

Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@register');

Route::get('/welcome', 'AuthController@welcome');

Route::get('/sample', 'SampleController@sample');


