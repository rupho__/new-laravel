<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function index(){
        // return "Olla, I am learning Laravel Home";
        return View('home');
    }
}
