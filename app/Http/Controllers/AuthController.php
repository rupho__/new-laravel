<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        //   return "Olla, I am learning Laravel Register";
        return View('register');
    }

    public function welcome(){
        //   return "Olla, I am learning Laravel Welcome";
        return View ('welcome_second');
    }
}
